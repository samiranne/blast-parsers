from random import randint
from matplotlib import numpy

fileInName = raw_input('Enter the name of the reciprocal BLAST file: ')
fileOutName = raw_input('Enter the desired name for the output file: ')
filein = open(fileInName,'r')
line = filein.readline()
line = filein.readline().strip()

lengths = []
invLengths = []
while line and line != '\n':
	print line.strip()
	fields = line.split(',')
	lengths.append((fields[1],fields[3]))
	invLengths.append((fields[3],fields[1]))
	line = filein.readline().strip()

newLengths = []
for x in range(len(lengths)):
	if randint(0,1) == 0:
		newLengths.append(lengths[x])
	else:
		newLengths.append(invLengths[x])

lengthDiffs = []
#fileout = open(fileOutName,'w')
for entry in newLengths:
	lengthDiffs.append(int(entry[0])-int(entry[1]))
	#fileout.write(str(entry[0]) + ',' + str(entry[1]) + '\n')
	print str(entry[0]) + ',' + str(entry[1]) + ',' + str(lengthDiffs[len(lengthDiffs)-1])
median = numpy.median(lengthDiffs)
print 'Median: ' + str(median)


