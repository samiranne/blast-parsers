#Takes input FASTA files, and CSV blast.

from Bio import SeqIO
from re import search, match


def queryLengths(queryFileName):
	queries = {}
	for entry in SeqIO.parse(queryFileName,'fasta'):
		queries[entry.id] = len(entry)
	print 'Sequence lengths recorded.'
	return queries

def dbLengths(dbFileName):
	dbseqs = {}
	for entry in SeqIO.parse(dbFileName,'fasta'):
		dbseqs[entry.id] = len(entry)
	print 'Sequence lengths recorded.'	
	return dbseqs

def topHits(alignFileName):
	filein = open(alignFileName,'r')
	line = filein.readline()
	parse = match('([^#^,]+)[^,]*,([^#^,]+)[^,]*,',line)
	hits = {}
	seqID = parse.group(1)
	hitID = parse.group(2)	
	hits[seqID] = hitID
	while line:
		parse = match('([^#^,]+)[^,]*,([^#^,]+)[^,]*,',line)
		if parse.group(1) != seqID:
			seqID = parse.group(1)
			hits[seqID] = parse.group(2)
		line = filein.readline()
	filein.close()
	print 'Top hits recorded.'
	return hits

def writeFile(hits,queries,dbseqs,outFileName):
	fileout = open(outFileName,'w')
	output = []
	fileout.write('QueryID,QueryLength,HitID,HitLength\n')	
	for entry in hits.keys():
		queryLength = queries[entry]
		hitLength = dbseqs[hits[entry]]
		output.append([entry,queryLength,hits[entry],hitLength])
		fileout.write(entry + ',' + str(queryLength) + ',' + hits[entry] + ',' + str(hitLength) + '\n')
	print 'Output written to ' + outFileName + '.'


queryFileName = raw_input('Enter the name of the query FASTA file: ')
dbFileName = raw_input('Enter the name of the FASTA file used to generate the database: ')
alignFileName = raw_input('Enter the name of the alignment file (CSV format): ')
outFileName = raw_input('Enter the desired name for the output file: ')

print 'Reading query FASTA file...'
queries = queryLengths(queryFileName)
print 'Reading database FASTA file...'
dbseqs = dbLengths(dbFileName)
print 'Parsing alignment file...'
hits = topHits(alignFileName)
writeFile(hits,queries,dbseqs,outFileName)

'''
queries = queryLengths('ptet_peptide.fasta')
dbseqs = dbLengths('tta_peptide.fasta')
hits = topHits('ptet_tta10-short')
writeFile(hits,queries,dbseqs,'parseblast2-01.txt')
'''

