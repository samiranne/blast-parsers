#Triple Reciprocals
#Finds 3-way reciprocal BLASTp matches.

from re import search
'''
infileNameA = raw_input('Enter the name of the first top hits file (Proteome A vs Proteome B): ')
infileNameB = raw_input('Enter the name of the second top hits file (Proteome B vs Proteome C): ')
infileNameC = raw_input('Enter the name of the third top hits file (Proteome C vs Proteome A): ')
outfileName = raw_input('Enter the desired name for the output file: ')
'''

infileNameA = 'ptet_pfal_top'
infileNameB = 'pfal_tta_top'
infileNameC = 'tta_ptet_top'
outfileName = 'ptet_pfal_tta'

print 'Reading first top hits file...'
fileA = open(infileNameA,'r')
fileA.readline()
matchAvB = {}
lengthsA = {} 
line = fileA.readline()
while line:
	fields = line.split(',')
	(protA,protB) = (fields[0],fields[2])
	lengthA = fields[1]
	matchAvB[protA] = protB
	lengthsA[protA] = lengthA
	line = fileA.readline()
fileA.close()

print 'Reading second top hits file...'
fileB = open(infileNameB,'r')
fileB.readline()
matchBvC = {}
lengthsB = {}
line = fileB.readline()
while line:
	fields = line.split(',')
	(protB,protC) = (fields[0],fields[2])
	lengthB = fields[1]
	matchBvC[protB] = protC
	lengthsB[protB] = lengthB
	line = fileB.readline()
fileB.close()

print 'Reading third top hits file...'
fileC = open(infileNameC,'r')
fileC.readline()
matchCvA = {}
lengthsC = {}
line = fileC.readline()
while line:
	fields = line.split(',')
	(protC,protA) = (fields[0],fields[2])
	lengthC = fields[1]
	matchCvA[protC] = protA
	lengthsC[protC] = lengthC
	line = fileC.readline()
fileC.close()
	
print 'Finding reciprocal matches...'
fileout = open(outfileName,'w')
fileout.write('NameA,LengthA,NameB,LengthB,NameC,LengthC\n')
for entry in matchAvB.keys():
	if matchAvB[entry] in matchBvC.keys() and matchBvC[matchAvB[entry]] in matchCvA.keys():
		if matchCvA[matchBvC[matchAvB[entry]]] == entry:
			nameA = entry
			nameB = matchAvB[entry]
			nameC = matchBvC[matchAvB[entry]] 
			lengthA = str(lengthsA[nameA])
			lengthB = str(lengthsB[nameB])
			lengthC = str(lengthsC[nameC])
			fileout.write(nameA + ',' + lengthA + ',' + nameB + ',' + lengthB + ',' + nameC + ',' + lengthC + '\n')

			
print 'Output file written to ' + outfileName + '.'



'''
AvB: (X,Y)
BvC: (Y,Z)
CvA: (Z,X)

matchAvB(X) = Y
matchBvC(Y) = Z
matchCvA(Z) = X

So if 
matchCvA(matchBvC(matchAvB(X))) = X
then is a 3-way reciprocal match.
'''
